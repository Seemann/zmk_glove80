# Seemann's ColemakDH keymap for Moergo Glove80

heavily inspired by: https://github.com/sunaku/glove80-keymaps

## Layout per layer

## Keymap 

https://my.glove80.com/#/layout/user/1f0958af-a0e1-422c-97cf-f23fef5adeb7

## Changes

## v11
- Numpad layer with inspiration from Sunaku
- Symbols layer from Sunaku
- sticky Shift on thumbs

### v10
- swap Symbols and Numpad layer keys
- 0 on thumb cluster
- tmux-Layer
- dwm-Layer
- Windows-Layer

### v9
- caps_word continue-list with German "-" and "_"
- change homerow to CGAS
- switch { and [ on symbols layer to match combos

### v8
- new symbols layer again, matching the Ferris Sweep layout
- Enter in numpad layer

### v7
- new Homerow CAGS
- changes to Sym-layer
- use new Homerow in Numpad and Nav layers
- swap z and y again

### v6
- Sunaku's Homerow mods

### v5
- hold on numpad keys to switch DWM tags
- DWM monitor switch on num layer (will create a specific DWM layer later)
- swap {} and []

### v4
- transparent thumb keys on sym and numpad layers
- combos for €, ö and ü on right hand only
- swap z and y
- add tmux-Session keybindings

### v3
- Change all transparent keys to &none
- Rename Windows layer to Shortcuts
- Add vim shortcuts
- new positions for ß and €
- combo for < and >

### v2
- Add layer for Windows specific keybindings
- Swap Semicolon and Quote
